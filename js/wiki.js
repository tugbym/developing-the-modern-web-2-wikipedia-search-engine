function get(url) {
    "use strict";
        
        //Make a new promise.
        var requestPromise = new Promise(function (resolve, reject) {

            //Do a GET request to the specified URL, with the mashape key.
            var req = new XMLHttpRequest();
            req.open('get', url);
            req.setRequestHeader("X-Mashape-Key", "oCdV3MjLj1mshtyIXwBVzBqRKtY9p1XJNiajsn1vsCETYVLwK3");

            //Got a response.
            req.onload = function () {
                if (req.status === 200) {
                    
                    //We got the response we wanted. So, let's resolve the promise.
                    resolve(req.response);
                    
                //We didn't get the response we wanted, so let's reject the promise.
                } else {
                    reject(new Error(req.statusText));
                }
            };

            //Didn't get a response.
            req.onerror = function () {
                reject(new Error("Network Error"));
            };

            //Send for a response.
            req.send();
        });

    //Return the response we got back from the request promise.
    return Promise.all([requestPromise]).then(function (results) {
        return results[0];
    });
}

function getJson(url) {
    "use strict";
    
    //Return the data back from the get function and parse the JSON response.
    return get(url).then(function (parse) {
        return (JSON.parse(parse).query.search);
    });
}

window.onload = function () {
    "use strict";
    
    //Start the search request every time the user changes the div's value.
    document.getElementById('searchInput').addEventListener("input", function () {
        
        //Creating the URL string, only getting the first 20 results from the Wikipedia API.
        var url = "https://community-wikipedia.p.mashape.com/api.php?srsearch=" + document.getElementById('searchInput').value + "&srlimit=20&format=json&action=query&list=search";
        
        //Start the spinner.
        document.querySelector('.spinner').style.display = 'block';
        
        //Call the getJson function.
        getJson(url).then(function (data) {
            var datasearch,
                title,
                urlTitle;
            
                //Empty out the search results div to stop concatenating the results.
                document.getElementById('searchResults').innerHTML = "";
            
                //Iterate through each search result in the array.
                for (datasearch in data) {
                    
                    //Ensure the result exists.
                    if (data.hasOwnProperty(datasearch)) {
                        
                        //Display the search results in the div.
                        title = data[datasearch].title;
                        urlTitle = title.replace(/ /g, "_");
                        document.getElementById('searchResults').innerHTML += "<br><a href=https://en.wikipedia.org/wiki/" + urlTitle + ">" + title + "</a>";
                        
                        //Stop the spinner.
                        document.querySelector('.spinner').style.display = 'none';
                    }
            }
        
        //There's been an error. So, let's stop the spinner and inform the user.
        }, Promise.resolve()).catch(function (err) {
            document.querySelector('.spinner').style.display = 'none';
            document.getElementById('searchResults').innerHTML = ("Error: " + err.message);
        });
    });
};