/*jslint browser: true, es5: true*/
/*global Promise, alert*/

function get(url) {
    "use strict";
    var key = "oCdV3MjLj1mshtyIXwBVzBqRKtY9p1XJNiajsn1vsCETYVLwK3",
        requestPromise = new Promise(function (resolve, reject) {

            var req = new XMLHttpRequest();
            req.open('get', url);
            req.setRequestHeader("X-Mashape-Key", key);

            req.onload = function () {
                if (req.status === 200) {
                    resolve(req.response);
                } else {
                    reject(new Error(req.statusText));
                }
            };

            req.onerror = function () {
                reject(new Error("Network Error"));
            };

            req.send();
        });

    return Promise.all([requestPromise]).then(function (results) {
        return results[0];
    });
}

function getJson(url) {
    "use strict";
    return get(url).then(function (parse) {
        return (JSON.parse(parse).query.search);
    });
}

window.onload = function () {
    "use strict";
    document.getElementById('searchInput').addEventListener("input", function () {
        
        var search = document.getElementById('searchInput').value,
            url = "https://community-wikipedia.p.mashape.com/api.php?srsearch=" + search + "&srlimit=20&format=json&action=query&list=search",
            link = "https://en.wikipedia.org/wiki/";
        
        document.querySelector('.spinner').style.display = 'block';
        
        getJson(url).then(function (data) {
            var datasearch,
                title,
                urlTitle;
            if (search === document.getElementById('searchInput').value) {
                document.getElementById("suggestions").innerHTML = "";
                for (datasearch in data) {
                    if (data.hasOwnProperty(datasearch)) {
                        title = data[datasearch].title;
                        urlTitle = title.replace(/ /g, "_");
                        document.getElementById("suggestions").innerHTML += "<br><a href=" + link + urlTitle + ">" + title + "</a>";
                        document.querySelector('.spinner').style.display = 'none';
                    }
                }
            }
        }, Promise.resolve()).catch(function (err) {
            document.getElementById("suggestions").innerHTML = ("Error: " + err.message);
        });
    });
};